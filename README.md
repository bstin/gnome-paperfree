# GNOME PaperFree
## Overview / Help

PaperFree is a GNOME-centric application that monitors a source directory for new files, and then OCR and catalogs the resulting files automatically.

By default PaperFree monitors $HOME/Scans directory, however this can be changed by editing the config file

Once PaperFree scans a document, the original will be removed and the OCR version will be moved to the .local directory

Files can be searched by simply using the default GNOME search window (eg. Super-Key and start typing). 
PaperFree exports its database as a GNOME search provider, clicking on a result will open that PDF

Search can be improved or narrowed down by using following conventions:

	* All searches are case-insensitive by default (ie. Searching for bank matches 'Bank', 'BANK', 'bAnK', etc) 
	* Using the '*' to extend words (ie. Searching for "bank*" finds "bank, banks, banking, etc")
	* Using the "-" operator to exclude terms (ie. Searching for "bank -foo", finds occurences of "bank" where word foo isn't present)
	* using the keyword NEAR('terms',distance) (ie. Searching for NEAR('bank america',3) finds occurences where bank/america are 3 words away or less)

### Requirements
	OS Level:
	* python3.5+
	* tesseract
	* tesseract-osd
	* ocrmypdf
	* pdftotext
	* md5sum
	Python Modules:
	* gi
	* threading
	* time
	* subprocess
	* pyinotify
	* os
	* GLib
	* Notify
	* GObject
	* ConfigParser

### Important Files and Directories

1. $HOME/.config/gnome-paperfree/paperfree.ini        <--- Configuration File
2. $HOME/.local/share/gnome-paperfree/                <--- Database and Processed Doc Storage
3. $HOME/.cache/gnome-paperfree/                      <--- Log and Debug location
4. $HOME/.local/share/applications/paperfree.desktop  <--- App Desktop File for GNOME