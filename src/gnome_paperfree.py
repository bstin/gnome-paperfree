#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- 
#
# main.py
# Copyright (C) 2018 Barry Stinson <barry@magicrd.com>
# 
# GNOME_PaperFree is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# GNOME_PaperFree is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
PaperFree Indexer, Search, and Notification Service
"""

APPNAME='PaperFree'
import gi, threading, time, subprocess, pyinotify, os, sqlite3,dbus,dbus.service
gi.require_version('Notify', '0.7')
from gi.repository import GLib, Notify, GObject
from dbus.mainloop.glib import DBusGMainLoop

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

def processFile(sfname):
	print("In processFile")
	basefile=os.path.basename(sfname)
	dfname=os.path.join(cfg['DEFAULT']['DestDir'],basefile)
	p1=subprocess.Popen(['/usr/bin/ocrmypdf',sfname,dfname], stdout=subprocess.PIPE)
	print(p1.communicate())
	md5=md5File(sfname)
	txt=pdf2Text(dfname)
	insertDb(dfname,md5,txt)
	os.remove(sfname)
	return dfname

def md5File(sfname):
	print("In md5File")
	proc=subprocess.Popen(['md5sum',sfname],stdout=subprocess.PIPE)
	h=proc.communicate()
	print(h[0])
	return h[0].split()[0].decode()

def pdf2Text(dfname):
	print("In pdf2Text")
	proc=subprocess.Popen(['pdftotext',dfname,'-'],stdout=subprocess.PIPE)
	t=proc.communicate()
	return t[0].decode().replace('\n','')

def initCfg():
	"""See if configuration paths / files exist, if not create them"""
	if os.path.isdir(os.path.join(HOMEDIR,CFGDIR))==False:
		os.mkdir(os.path.join(HOMEDIR,CFGDIR))
		cfg['DEFAULT']={'SourceDir':os.path.join(HOMEDIR,'Scans'),
			'DestDir':os.path.join(HOMEDIR,LOCALDIR,'docs'),
			'DatabaseFile':os.path.join(HOMEDIR,LOCALDIR,DBFILE),
			'LogFile':os.path.join(HOMEDIR,CACHEDIR,LOGFILE)}
		with open(os.path.join(HOMEDIR,CFGDIR,CFGFILE), 'w') as configfile:
			cfg.write(configfile)
	if os.path.isdir(os.path.join(HOMEDIR,LOCALDIR))==False:
		os.mkdir(os.path.join(HOMEDIR,LOCALDIR))
		os.mkdir(os.path.join(HOMEDIR,LOCALDIR,'docs'))
	if os.path.isdir(os.path.join(HOMEDIR,LOCALDIR,'docs'))==False:
		os.mkdir(os.path.join(HOMEDIR,LOCALDIR,'docs'))
	if os.path.isdir(os.path.join(HOMEDIR,'Scans'))==False:
		os.mkdir(os.path.join(HOMEDIR,'Scans'))
	if os.path.isfile(os.path.join(HOMEDIR,LOCALDIR,DBFILE))==False:
		createDb()

def createDb():
	db=sqlite3.connect(os.path.join(HOMEDIR,LOCALDIR,DBFILE))
	cur=db.cursor()
	cur.execute('CREATE VIRTUAL TABLE scans USING FTS5 (file,hash,text);')
	db.commit()
	db.close()

def insertDb(fname,hsh,text):
	print("type: "+str(type(hsh)))
	db=sqlite3.connect(os.path.join(HOMEDIR,LOCALDIR,DBFILE))
	cur=db.cursor()
	rowcount=cur.execute("SELECT COUNT(*) FROM scans WHERE hash=?;",(hsh,)).fetchone()[0]
	if (rowcount>0):
		print("File already exist, only updating the values")
	else:
		print("New file, adding to db")
		cur.execute('INSERT INTO scans (file,hash,text) VALUES (?,?,?)',(fname,hsh,text))
	db.commit()
	db.close()

HOMEDIR=os.path.expanduser('~')
LOCALDIR='.local/share/gnome-paperfree'
CACHEDIR='.cache/gnome-paperfree/'
CFGDIR='.config/gnome-paperfree/'
DBFILE='paperfree.db'
LOGFILE='paperfree.log'
CFGFILE='paperfree.ini'
cfg=ConfigParser()
initCfg()
cfg.read(os.path.join(HOMEDIR,CFGDIR,CFGFILE))

search_bus_name = 'org.gnome.Shell.SearchProvider2'
sbn = dict(dbus_interface=search_bus_name)

class SearchPaperFreeService(dbus.service.Object):
	""" The pass search daemon.
	This service is started through DBus activation by calling the
	:meth:`Enable` method, and stopped with :meth:`Disable`.
	"""
	bus_name = 'org.gnome.PaperFree.SearchProvider'

	_object_path = '/' + bus_name.replace('.', '/')

	def __init__(self):
		DBusGMainLoop(set_as_default=True)
		print('In Dbus-init')
		self.session_bus = dbus.SessionBus()
		bus_name = dbus.service.BusName(self.bus_name, bus=self.session_bus)
		dbus.service.Object.__init__(self, bus_name, self._object_path)

	@dbus.service.method(in_signature='sasu', **sbn)
	def ActivateResult(self, id, terms, timestamp):
		print(id,terms)
		return self.get_result_set(terms)

	@dbus.service.method(in_signature='as', out_signature='as', **sbn)
	def GetInitialResultSet(self, terms):
		return self.get_result_set(terms)

	@dbus.service.method(in_signature='as', out_signature='aa{sv}', **sbn)
	def GetResultMetas(self, ids):
		return [dict(id=id, name=id,) for id in ids]

	@dbus.service.method(in_signature='asas', out_signature='as', **sbn)
	def GetSubsearchResultSet(self, previous_results, new_terms):
		return self.get_result_set(new_terms)

	@dbus.service.method(in_signature='asu', terms='as', timestamp='u', **sbn)
	def LaunchSearch(self, terms, timestamp):
		pass

	def get_result_set(self,terms):
		return "MyTerms: %s" % (terms[0])


class NotifyEventHandler(pyinotify.ProcessEvent):
	def __init__(self):
		print("In handler-init")
		#SearchPaperFreeService()
		self.noti = None
		self.last_file=None
		Notify.init(APPNAME)
		self.noti = Notify.Notification.new('PaperFree')
		self.noti.add_action('clicked', 'Open', self.notification_callback, None)

	def notification_callback(self, notification, action_name, data):
		print("In handler-callback")
		print(action_name,data)
		subprocess.Popen(['xdg-open', self.last_file])

	def process_IN_ACCESS(self, event):
		print("ACCESS event:", event.pathname)

	def process_IN_ATTRIB(self, event):
		print("ATTRIB event:", event.pathname)

	def process_IN_CLOSE_NOWRITE(self, event):
		print("CLOSE_NOWRITE event:", event.pathname)

	def process_IN_CLOSE_WRITE(self, event):
		print("In handler-close_write")
		print("CLOSE_WRITE event:", event.pathname)
		self.last_file=processFile(event.pathname)
		self.noti.update('PaperFree File Added',self.last_file)
		self.noti.show()

	def process_IN_CREATE(self, event):
		print("CREATE event:", event.pathname)

	def process_IN_DELETE(self, event):
		print("DELETE event:", event.pathname)

	def process_IN_MODIFY(self, event):
		print("MODIFY event:", event.pathname)

	def process_IN_OPEN(self, event):
		print("OPEN event:", event.pathname)


wm = pyinotify.WatchManager()
wm.add_watch(cfg['DEFAULT']['SourceDir'], pyinotify.ALL_EVENTS, rec=True)


# event handler
eh = NotifyEventHandler()

# notifier
notifier = pyinotify.Notifier(wm, eh)
#notifier.loop()
t=threading.Thread(target=notifier.loop)
t.daemon=True
t.start()
#s=SearchPaperFreeService()

#dbus.set_default_main_loop(SearchPaperFreeService)
#app = App()
spfs=SearchPaperFreeService()
#GObject.MainLoop.run()
GLib.MainLoop().run()
